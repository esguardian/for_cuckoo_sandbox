#!/bin/bash
# esguardian@outlook.com
# install cuckoo-1.3-optiv on debian jessie_amd64
#
s=$(egrep -c '(vmx|svm)' /proc/cpuinfo)
if [ $s -lt 2 ];
then
    echo "You must have at least 2 processors with hardware virtualization support\nI found $s\nFirst you must check your BIOS or hypervisor config.\nNow I stop.\nSee you later..."
    return;
else
    echo "Found $s processors with hardware virtualization.\nContinue\n"
fi	
echo "End of test. Now starting ..."
#-------------------------------------------#
# Устанавливаем Cuckoo Sandbox 
# Тестировалось на Debian 8 (jessie_amd64)
#-------------------------------------------#
#Установим зависимости
cd /tmp
apt-get update
apt-get install  git automake mongodb mingw32 dkms unzip wget python python-sqlalchemy python-bson python-pip python-dpkt python-jinja2 python-magic python-gridfs python-libvirt python-bottle python-pefile python-chardet -y
apt-get install python-dev libxml2-dev libxslt1-dev libevent-dev libpcre3 libpcre3-dev zlib1g-dev libtool libpcre++-dev -y
debconf-set-selections <<< 'mariadb-server-5.5 mysql-server/root_password password MySeCRretpaSSw0rd'
debconf-set-selections <<< 'mariadb-server-5.5 mysql-server/root_password_again password MySeCRretpaSSw0rd'
apt-get install mariadb-server -y
apt-get install python-mysqldb -y  
apt-get install swig libssl-dev -y
apt-get install clamav-daemon python-geoip geoip-database -y
pip install django       
pip install py3compat
pip install pygal
pip install m2crypto
pip install dnspython
#pip install clamd
pip install django-ratelimit
pip install pycrypto
pip install rarfile
pip install jsbeautifier
apt-get install wkhtmltopdf xvfb xfonts-100dpi -y 
pip install lxml
pip install cybox==2.1.0.9
pip install maec==4.1.0.11
pip install pymongo
pip install --pre pype32
#
#Установим SSDEEP
#
apt-get install ssdeep python-pyrex subversion libfuzzy-dev -y
svn checkout http://pyssdeep.googlecode.com/svn/trunk/ pyssdeep
cd pyssdeep
python setup.py build
python setup.py install
pip install pydeep
#
#Установим Yara
#
cd /tmp
wget https://github.com/plusvic/yara/archive/v3.4.0.tar.gz
tar xzf v3.4.0.tar.gz
cd yara-3.4.0
chmod +x build.sh
./build.sh
make install
cd yara-python
python setup.py build
python setup.py install
#
#Установим Distorm3
#
cd /tmp
wget http://distorm.googlecode.com/files/distorm3.zip
unzip distorm3.zip
cd distorm3/
python setup.py build
python setup.py install
#
#Устанавливаем Volatility
#
add-apt-repository ppa:pi-rho/security -y
apt-get update
apt-get install volatility -y
#
#
cd /opt
mkdir -p /opt/cuckoo/
cd /opt/cuckoo

git clone https://github.com/brad-accuvant/cuckoo-modified .
#Устанавливаем сигнатуры для модифицированной Cuckoo 
cd /opt/cuckoo
./utils/community.py --signatures --force
#Ставим Zer0m0n
cd /tmp
git clone https://github.com/zer0box/zer0m0n
cd zer0m0n/bin
cp cuckoo.patch /opt/cuckoo
cd /opt/cuckoo
patch -p1 < ./cuckoo.patch
cp /tmp/zer0m0n/bin/logs_dispatcher.exe /opt/cuckoo/analyzer/windows/dll/
cp /tmp/zer0m0n/bin/zer0m0n.sys /opt/cuckoo/analyzer/windows/dll/
cp -rf /tmp/zer0m0n/signatures/* /opt/cuckoo/modules/signatures/


#-------------------------------------------#
# Устанавливаем и настраиваем Virtualbox
#-------------------------------------------#
cd /tmp
wget http://download.virtualbox.org/virtualbox/5.0.0/virtualbox-5.0_5.0.0-101573~Debian~jessie_amd64.deb
dpkg -i virtualbox-5.0_5.0.0-101573~Debian~jessie_amd64.deb
apt-get update
apt-get install -f -y
wget http://download.virtualbox.org/virtualbox/5.0.0/Oracle_VM_VirtualBox_Extension_Pack-5.0.0-101573.vbox-extpack
vboxmanage extpack install Oracle_VM_VirtualBox_Extension_Pack-5.0.0-101573.vbox-extpack
cd /home/cuckoo
wget http://download.virtualbox.org/virtualbox/5.0.0/VBoxGuestAdditions_5.0.0.iso
#конфигурируем Виртуальную машину
cd /tmp
vboxmanage hostonlyif create
vboxmanage import /home/cuckoo/win7-cuck.ova --vsys 0 --vmname Windows7
vboxmanage modifyvm "Windows7" --nic1 hostonly --hostonlyadapter1 vboxnet0 --nicpromisc1 allow-all --hwvirtex on --vtxvpid on
#Настраиваем общие папки
mkdir -p /opt/cuckoo/shares/setup
mkdir -p /opt/cuckoo/shares/Windows7
vboxmanage sharedfolder remove "Windows7" --name "Windows7"
vboxmanage sharedfolder remove "Windows7" --name setup
vboxmanage sharedfolder add "Windows7" --name "Windows7" --hostpath /opt/cuckoo/shares/Windows7 --automount
vboxmanage sharedfolder add "Windows7" --name setup --hostpath /opt/cuckoo/shares/setup --automount --readonly
vboxmanage modifyvm "Windows7" --nictrace1 on --nictracefile1 /opt/cuckoo/shares/Windows7/dump.pcap
cp /opt/cuckoo/agent/agent.py /opt/cuckoo/shares/setup/agent.pyw
#включаем доступ по RDP 
vboxmanage modifyvm "Windows7" --vrde on --vrdeport 7001
vboxmanage startvm "Windows7" --type headless
#настраиваем iptables
iptables -A FORWARD -o eth0 -i vboxnet0 -s 192.168.56.0/24 -m conntrack --ctstate NEW -j ACCEPT
iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A POSTROUTING -t nat -j MASQUERADE  
sysctl -w net.ipv4.ip_forward=1 
echo 'net.ipv4.ip_forward=1' >> /etc/sysctl.conf 
#Автозагрузка правил Iptables
mkdir -p /etc/iptables/
iptables-save > /etc/iptables/rules.v4
echo 'post-up /sbin/iptables-restore < /etc/iptables/rules.v4' >> /etc/network/interfaces 
#
#Делаем паузу для проверки и настройки виртуальной машины
#
read -p "Use RDP:7001 to connect VM and check configuration (may be update and reboot needed) Than press [ENTER] to continue,...: "
vboxmanage snapshot "Windows7" take "Windows7Snap01" --pause
vboxmanage controlvm "Windows7" poweroff
#
#
#Настраиваем tcpdump
apt-get install libcap2-bin -y 
apt-get install tcpdump -y
setcap cap_net_raw,cap_net_admin=eip /usr/sbin/tcpdump
getcap /usr/sbin/tcpdump
#настраиваем БД Cuckoo
mysql -uroot -pMySeCRretpaSSw0rd -e "create database cuckoo"
mysql -uroot -pMySeCRretpaSSw0rd -e "grant all privileges on cuckoo.* to cuckoo@localhost identified by 'cuck00pass'"
mysql -u root -pMySeCRretpaSSw0rd -e "flush privileges"
#Настраиваем конфиг Cuckoo
sed -i -e "s@connection =@connection = mysql://cuckoo:cuck00pass\@localhost/cuckoo@" /opt/cuckoo/conf/cuckoo.conf
sed -i -e "s@memory_dump = off@memory_dump = on@" /opt/cuckoo/conf/cuckoo.conf
sed -i -e "s@delete_memdump = no@delete_memdump = yes@" /opt/cuckoo/conf/memory.conf
# У меня Win7SP1x86. Если у вас по другому укажите правильный профиль для Volatility
sed -i -e "s@guest_profile = WinXPSP2x86@guest_profile = Win7SP1x86@" /opt/cuckoo/conf/memory.conf
# Настраиваем процессинг и репортинг
sed -i -e "1,/suricata/ s@enabled = no@enabled = yes@" /opt/cuckoo/conf/processing.conf
sed -i -e "1,/reportpdf/ s@enabled = no@enabled = yes@" /opt/cuckoo/conf/reporting.conf
sed -i -e "/maec41/,/elasticsearchdb/ s@enabled = no@enabled = yes@" /opt/cuckoo/conf/reporting.conf
# Настраиваем Virtualbox
sed -i -e "s@mode = gui@mode = headless@" /opt/cuckoo/conf/virtualbox.conf
sed -i -e "s@cuckoo1@Windows7@" /opt/cuckoo/conf/virtualbox.conf
sed -i -e "s@# snapshot = Snapshot1@snapshot = Windows7Snap01@" /opt/cuckoo/conf/virtualbox.conf
# Вносим изменения в mashinery.py там баг с неправильной командой для vboxmanage
sed -i -e "s@dumpguestcore@dumpvmcore@" /opt/cuckoo/modules/machinery/virtualbox.py
#Настраиваем Веб сервер
apt-get install apache2 -y
mv /etc/apache2/sites-enabled/000-default.conf /etc/apache2/sites-enabled/000-default.conf.bak
cat > /etc/apache2/sites-enabled/cuckoo.conf <<DELIM
<VirtualHost *:80>
ServerName cuckoo-optiv.local
ServerAdmin webmaster@localhost
DocumentRoot /opt/cuckoo/web
ErrorLog /var/log/apache2/error.log
CustomLog /var/log/apache2//access.log combined
WSGIScriptAlias / /opt/cuckoo/web/web/wsgi.py
<Directory /opt/cuckoo/web/web>
<Files wsgi.py>
Require all granted
</Files>
</Directory>
Alias /static /opt/cuckoo/web/static
<Directory /opt/cuckoo/web/static/>
Require all granted
</Directory>
</VirtualHost>
DELIM
apt-get install libapache2-mod-wsgi -y
mv /opt/cuckoo/web/web/wsgi.py /opt/cuckoo/web/web/wsgi.py.bak
cat > /opt/cuckoo/web/web/wsgi.py  <<DELIM
import os, sys
sys.path.append('/opt/cuckoo')
sys.path.append('/opt/cuckoo/web')
os.chdir('/opt/cuckoo/web/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "web.settings")
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
DELIM

#Настраиваем автозагрузку интерфейса vboxnet0
sed -i -e "s@exit 0@@" /etc/rc.local
echo 'VBoxManage list vms > /dev/null' >> /etc/rc.local
echo 'ifconfig vboxnet0 192.168.56.1' >> /etc/rc.local
echo 'exit 0' >> /etc/rc.local
#Настраиваем автозагрузку Cuckoo
apt-get install supervisor -y
cat > /etc/supervisor/conf.d/cuckoo.conf <<DELIM
[program:cuckoo]
command=python cuckoo.py
directory=/opt/cuckoo

[program:cuckoo-api]
command=python api.py
directory=/opt/cuckoo/utils
DELIM
supervisord -c /etc/supervisor/supervisord.conf
supervisorctl -c /etc/supervisor/supervisord.conf reload


chmod -R 777 /opt/cuckoo/web

#!/bin/bash
# ESguardian@outlook.com
# Это мой вариант скрипта изначально созданного пользователем Хабра Libert
# и опубликованного здесь: http://habrahabr.ru/post/234467/
#
# и возможно вам стоит поменять MySeCRretpaSSw0rd на свой собственный.
#
s=$(egrep -c '(vmx|svm)' /proc/cpuinfo)
if [ $s -lt 2 ];
then
    echo "You must have at least 2 processors with hardware virtualization support\nI found $s\nFirst you must check your BIOS or hypervisor config.\nNow I stop.\nSee you later..."
    return;
else
    echo "Found $s processors with hardware virtualization.\nContinue\n"
fi	
echo "End of test. Now starting ..."
#-------------------------------------------#
# Устанавливаем Cuckoo Sandbox 
# Тестировалось на Debian 8 (jessie_amd64)
#-------------------------------------------#
#Установим зависимости
cd /tmp
apt-get update
apt-get install  git automake mongodb mingw32 dkms unzip wget python python-sqlalchemy python-bson python-pip python-dpkt python-jinja2 python-magic python-gridfs python-libvirt python-bottle python-pefile python-chardet -y
apt-get install python-dev libxml2-dev libxslt1-dev libevent-dev libpcre3 libpcre3-dev zlib1g-dev libtool libpcre++-dev -y
debconf-set-selections <<< 'mariadb-server-5.5 mysql-server/root_password password MySeCRretpaSSw0rd'
debconf-set-selections <<< 'mariadb-server-5.5 mysql-server/root_password_again password MySeCRretpaSSw0rd'
apt-get install mariadb-server -y
apt-get install python-mysqldb 
#Установим компоненты
pip install lxml
pip install cybox==2.0.1.4
pip install maec==4.0.1.0
pip install django
pip install py3compat
pip install pymongo
#Установим SSDEEP
apt-get install ssdeep python-pyrex subversion libfuzzy-dev -y
svn checkout http://pyssdeep.googlecode.com/svn/trunk/ pyssdeep
cd pyssdeep
python setup.py build
python setup.py install
pip install pydeep
#Установим Yara
cd /tmp
wget https://github.com/plusvic/yara/archive/v3.4.0.tar.gz
tar xzf v3.4.0.tar.gz
cd yara-3.4.0
chmod +x build.sh
./build.sh
make install
cd yara-python
python setup.py build
python setup.py install
#Установим Distorm3
cd /tmp
wget http://distorm.googlecode.com/files/distorm3.zip
unzip distorm3.zip
cd distorm3/
python setup.py build
python setup.py install
#Устанавливаем Volatility
add-apt-repository ppa:pi-rho/security -y
apt-get update
apt-get install volatility -y
#Устанавливаем Cuckoo

cd /home
wget http://downloads.cuckoosandbox.org/1.2/cuckoo_1.2.tar.gz
tar xzf cuckoo_1.2.tar.gz
#Устанавливаем сигнатуры Cuckoo (https://github.com/cuckoobox/community)
cd /home/cuckoo
./utils/community.py --signatures --force
#Ставим Zer0m0n
cd /tmp
git clone https://github.com/zer0box/zer0m0n
cd zer0m0n/bin
cp cuckoo.patch /home/cuckoo
cd /home/cuckoo
patch -p1 < ./cuckoo.patch
cp /tmp/zer0m0n/bin/logs_dispatcher.exe /home/cuckoo/analyzer/windows/dll/
cp /tmp/zer0m0n/bin/zer0m0n.sys /home/cuckoo/analyzer/windows/dll/
cp -rf /tmp/zer0m0n/signatures/* /home/cuckoo/modules/signatures/
#Дополнительные сигнатуры PEiD
cd /tmp
wget http://research.pandasecurity.com/blogs/images/userdb.txt
mv userdb.txt /home/cuckoo/data/peutils/UserDB.TXT
#ClamAV  сигнатуры для Yara
cd /tmp
apt-get install clamav -y
wget http://db.local.clamav.net/main.cvd
wget http://db.local.clamav.net/daily.cvd
sigtool -u main.cvd
sigtool -u daily.cvd
wget https://malwarecookbook.googlecode.com/svn-history/trunk/3/3/clamav_to_yara.py
python clamav_to_yara.py -f main.ndb -o main.yar
python clamav_to_yara.py -f daily.ndb -o daily.yar
#фикс, в ClamAV есть сигнатура EOL_0_94_2, которая конвертируется, но не валидна для Yara
RM_EOL=$(sed -n '/EOL_0_94_2/{=}' main.yar)
for n in {1..8}; do sed -i "${RM_EOL}d" main.yar; done
mkdir /home/cuckoo/data/yara/clamav
mv *.yar /home/cuckoo/data/yara/clamav/
git clone https://github.com/AlienVault-Labs/AlienVaultLabs.git
mv AlienVaultLabs/malware_analysis/ /home/cuckoo/data/yara/
mv /home/cuckoo/data/yara/index_binary.yar /home/cuckoo/data/yara/index_binary.yar.bak
cat > /home/cuckoo/data/yara/index_binary.yar <<DELIM
include "signatures/embedded.yar"
include "signatures/vmdetect.yar"
include "clamav/main.yar"
include "clamav/daily.yar"
include "malware_analysis/CommentCrew/apt1.yara"
include "malware_analysis/FPU/fpu.yar"
include "malware_analysis/Georbot/GeorBotBinary.yara"
include "malware_analysis/Georbot/GeorBotMemory.yara"
include "malware_analysis/Hangover/hangover.yar"
include "malware_analysis/KINS/kins.yar"
include "malware_analysis/OSX_Leverage/leverage.yar"
include "malware_analysis/TheMask_Careto/mask.yar"
include "malware_analysis/Urausy/urausy_skypedat.yar"
DELIM

#-------------------------------------------#
# Устанавливаем и настраиваем Virtualbox
#-------------------------------------------#
cd /tmp
wget http://download.virtualbox.org/virtualbox/5.0.0/virtualbox-5.0_5.0.0-101573~Debian~jessie_amd64.deb
dpkg -i virtualbox-5.0_5.0.0-101573~Debian~jessie_amd64.deb
apt-get update
apt-get install -f -y
wget http://download.virtualbox.org/virtualbox/5.0.0/Oracle_VM_VirtualBox_Extension_Pack-5.0.0-101573.vbox-extpack
vboxmanage extpack install Oracle_VM_VirtualBox_Extension_Pack-5.0.0-101573.vbox-extpack
cd /home/cuckoo
wget http://download.virtualbox.org/virtualbox/5.0.0/VBoxGuestAdditions_5.0.0.iso
#конфигурируем Виртуальную машину
cd /tmp
vboxmanage hostonlyif create
vboxmanage import /home/cuckoo/win7-cuck.ova --vsys 0 --vmname Windows7
vboxmanage modifyvm "Windows7" --nic1 hostonly --hostonlyadapter1 vboxnet0 --nicpromisc1 allow-all --hwvirtex on --vtxvpid on
#Настраиваем общие папки
mkdir -p /home/cuckoo/shares/setup
mkdir -p /home/cuckoo/shares/Windows7
vboxmanage sharedfolder add "Windows7" --name "Windows7" --hostpath /home/cuckoo/shares/Windows7 --automount
vboxmanage sharedfolder add "Windows7" --name setup --hostpath /home/cuckoo/shares/setup --automount --readonly
vboxmanage modifyvm "Windows7" --nictrace1 on --nictracefile1 /home/cuckoo/shares/Windows7/dump.pcap
cp /home/cuckoo/agent/agent.py /home/cuckoo/shares/setup/agent.pyw
#включаем доступ по RDP 
vboxmanage modifyvm "Windows7" --vrde on --vrdeport 7001
vboxmanage startvm "Windows7" --type headless
#делаем паузу, чтобы машина запустилась и приработалась
sleep 180
vboxmanage snapshot "Windows7" take "Windows7Snap01" --pause
vboxmanage controlvm "Windows7" poweroff

#настраиваем iptables
iptables -A FORWARD -o eth0 -i vboxnet0 -s 192.168.56.0/24 -m conntrack --ctstate NEW -j ACCEPT
iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A POSTROUTING -t nat -j MASQUERADE  
sysctl -w net.ipv4.ip_forward=1 
echo 'net.ipv4.ip_forward=1' >> /etc/sysctl.conf 
#Автозагрузка правил Iptables
mkdir -p /etc/iptables/
iptables-save > /etc/iptables/rules.v4
echo 'post-up /sbin/iptables-restore < /etc/iptables/rules.v4' >> /etc/network/interfaces 

#Настраиваем tcpdump
apt-get install tcpdump
setcap cap_net_raw,cap_net_admin=eip /usr/sbin/tcpdump
getcap /usr/sbin/tcpdump
#настраиваем БД Cuckoo
mysql -uroot -pMySeCRretpaSSw0rd -e "create database cuckoo"
mysql -uroot -pMySeCRretpaSSw0rd -e "grant all privileges on cuckoo.* to cuckoo@localhost identified by 'cuck00pass'"
mysql -u root -pMySeCRretpaSSw0rd -e "flush privileges"
#Настраиваем конфиг Cuckoo
sed -i -e "s@connection =@connection = mysql://cuckoo:cuck00pass\@localhost/cuckoo@" /home/cuckoo/conf/cuckoo.conf
sed -i -e "s@memory_dump = off@memory_dump = on@" /home/cuckoo/conf/cuckoo.conf
sed -i -e "s@default = 120@default = 240@" /home/cuckoo/conf/cuckoo.conf
sed -i -e "s@critical = 600@critical = 1200@" /home/cuckoo/conf/cuckoo.conf
sed -i -e "s@vm_state = 300@vm_state = 600@" /home/cuckoo/conf/cuckoo.conf
sed -i -e "s@delete_memdump = no@delete_memdump = yes@" /home/cuckoo/conf/memory.conf
sed -i -e "s@enabled = no@enabled = yes@" /home/cuckoo/conf/processing.conf
sed -i -e "s@enabled = no@enabled = yes@" /home/cuckoo/conf/reporting.conf
sed -i -e "s@enabled = no@enabled = yes@" /home/cuckoo/conf/processing.conf
sed -i -e "s@mode = gui@mode = headless@" /home/cuckoo/conf/virtualbox.conf
sed -i -e "s@cuckoo1@Windows7@" /home/cuckoo/conf/virtualbox.conf
#Настраиваем Веб сервер
apt-get install apache2 -y
mv /etc/apache2/sites-enabled/000-default.conf /etc/apache2/sites-enabled/000-default.conf.bak
cat > /etc/apache2/sites-enabled/cuckoo.conf <<DELIM
<VirtualHost *:80>
ServerName cuckoo.local
ServerAdmin webmaster@localhost
DocumentRoot /home/cuckoo/web
ErrorLog /var/log/apache2/error.log
CustomLog /var/log/apache2//access.log combined
WSGIScriptAlias / /home/cuckoo/web/web/wsgi.py
<Directory /home/cuckoo/web/web>
<Files wsgi.py>
Require all granted
</Files>
</Directory>
Alias /static /home/cuckoo/web/static
<Directory /home/cuckoo/web/static/>
Require all granted
</Directory>
</VirtualHost>
DELIM
aptitude install libapache2-mod-wsgi -y
mv /home/cuckoo/web/web/wsgi.py /home/cuckoo/web/web/wsgi.py.bak
cat > /home/cuckoo/web/web/wsgi.py  <<DELIM
import os, sys
sys.path.append('/home/cuckoo')
sys.path.append('/home/cuckoo/web')
os.chdir('/home/cuckoo/web/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "web.settings")
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
DELIM
chown -R cuckoo:cuckoo /home/cuckoo/
#Настраиваем автозагрузку интерфейса vboxnet0
sed -i -e "s@exit 0@@" /etc/rc.local
echo 'VBoxManage list vms > /dev/null' >> /etc/rc.local
echo 'ifconfig vboxnet0 192.168.56.1' >> /etc/rc.local
echo 'exit 0' >> /etc/rc.local
#Настраиваем автозагрузку Cuckoo
apt-get install supervisor -y
cat > /etc/supervisor/conf.d/cuckoo.conf <<DELIM
[program:cuckoo]
command=python cuckoo.py
directory=/home/cuckoo

[program:cuckoo-api]
command=python api.py
directory=/home/cuckoo/utils
DELIM
supervisord -c /etc/supervisor/supervisord.conf
supervisorctl -c /etc/supervisor/supervisord.conf reload


#Дополнительные сигнатуры PEiD
cd /tmp
wget http://research.pandasecurity.com/blogs/images/userdb.txt
mv userdb.txt /home/cuckoo/data/peutils/UserDB.TXT

usermod -a -G vboxusers cuckoo 
chown -R cuckoo:cuckoo /home/cuckoo/
chmod -R 777 /home/cuckoo/web


